//
//  DegreesConverter.swift
//  CircleProgressBarClearScore
//
//  Created by Javier Servate on 12/04/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class DegreesConverter: NSObject {
    func formatToRadians(degress: Double) -> Double {
        return (degress * .pi) / 180
    }
    
    func formatToDegrees(radians: Double) -> Double {
        return (radians * 180) / .pi
    }
}

//
//  Constants.swift
//  CircleProgressBarClearScore
//
//  Created by Javier Servate on 11/04/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

//It is useful to have a constants file that contains all the literal strings so in case the api path changes for example, it just needs to be changed here and not in 10 different files where it can be used along the code. That's the reason I also use the SwiftGen framework, although that contains the visual strings (like for labels, buttons and so) declared in the Localizable.strings
struct Constants {
    struct APIPaths {
        static let baseUrl = "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com"
        static let scoreValues = "/prod/mockcredit/values"
    }
    
    static let apiRequestError = "Request did not succeed"
}

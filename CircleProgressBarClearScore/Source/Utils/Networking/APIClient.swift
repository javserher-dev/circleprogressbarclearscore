//
//  APIClient.swift
//  CircleProgressBarClearScore
//
//  Created by Javier Servate on 11/04/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import Alamofire

final class APIClient: NSObject {
    
    //Singleton instance because we are going to use it so much and there is no point on having multiple instances all over the code
    static let shared = APIClient()
    
    func performRequest<T: Decodable>(router: APIRouter, completion: @escaping (_: T?) -> Void) {
        Alamofire.request(router).responseData(completionHandler: { [weak self] result in
            guard let response = result.response, let data = result.data, (200...300).contains(response.statusCode) else {
                debugPrint(Constants.apiRequestError)
                completion(nil)
                return
            }
            //Parse the JSON data to the appropiate class of the Model passed in the parameters
            completion(try? JSONDecoder().decode(T.self, from: data))
        })
    }
}

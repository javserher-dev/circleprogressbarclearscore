//
//  APIRouter.swift
//  CircleProgressBarClearScore
//
//  Created by Javier Servate on 11/04/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import Alamofire

//Here we define our posible request scenarios and for each of them we set their own parameters, headers and so. In this case we just request the score. Finally we return a customized URL request depending on what resource we are requesting or accessing.
enum APIRouter: URLRequestConvertible {
    case score
    
    var method: HTTPMethod {
        switch self {
        case .score: return .get
        }
    }
    
    var path: String {
        switch self {
        case .score: return Constants.APIPaths.scoreValues
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .score: return ["Accept": "application/json"]
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .score: return nil
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.APIPaths.baseUrl.asURL()
        var request = URLRequest(url: url.appendingPathComponent(path))
        request.allHTTPHeaderFields = headers
        request.httpMethod = method.rawValue
        switch method {
        case .get: return try URLEncoding().encode(request, with: parameters)
        default: return try JSONEncoding().encode(request, with: parameters)
        }
    }
}

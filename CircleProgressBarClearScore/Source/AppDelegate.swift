//
//  AppDelegate.swift
//  CircleProgressBarClearScore
//
//  Created by Javier Servate on 11/04/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: ScoreDashboardViewController())
        window?.makeKeyAndVisible()
        return true
    }
}

// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {

  internal enum Alert {
    internal enum Description {
      /// Could not get the info.\nRetry?
      internal static let requestError = L10n.tr("Localizable", "Alert.Description.requestError")
    }
    internal enum Titles {
      /// Error
      internal static let error = L10n.tr("Localizable", "Alert.Titles.error")
    }
  }

  internal enum Buttons {
    /// Cancel
    internal static let cancel = L10n.tr("Localizable", "Buttons.cancel")
    /// Ok
    internal static let ok = L10n.tr("Localizable", "Buttons.ok")
  }

  internal enum Labels {
    /// Loading info
    internal static let loading = L10n.tr("Localizable", "Labels.loading")
    internal enum NavigationBar {
      /// Dashboard
      internal static let scoreDashboard = L10n.tr("Localizable", "Labels.navigationBar.ScoreDashboard")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}

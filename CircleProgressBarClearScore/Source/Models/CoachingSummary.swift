//
//  CoachingSummary.swift
//  CircleProgressBarClearScore
//
//  Created by Javier Servate on 12/04/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

struct CoachingSummary: Codable {
    let activeTodo, activeChat: Bool
    let numberOfTodoItems, numberOfCompletedTodoItems: Int
    let selected: Bool
}

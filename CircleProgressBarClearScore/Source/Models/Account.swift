//
//  Account.swift
//  CircleProgressBarClearScore
//
//  Created by Javier Servate on 12/04/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

//Each class of the model should conform the Codable protocol in order to be able to be coded and decoded when requesting/sending to the server
struct Account: Codable {
    let accountIDVStatus: String
    let creditReportInfo: CreditReportInfo
    let dashboardStatus, personaType: String
    let coachingSummary: CoachingSummary
    let augmentedCreditScore: String?
}

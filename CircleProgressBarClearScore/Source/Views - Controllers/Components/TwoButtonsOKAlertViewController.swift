//
//  TwoButtonsAlertViewController.swift
//  CircleProgressBarClearScore
//
//  Created by Javier Servate on 12/04/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

typealias AlertActionCompletion = (UIAlertAction) -> Void

final class TwoButtonsAlertViewController: UIAlertController {
    
    func setupAlert(leftButtonCompletion: @escaping AlertActionCompletion, rightButtonCompletion: @escaping AlertActionCompletion) {
        self.addAction(UIAlertAction(title: L10n.Buttons.cancel, style: .default, handler: leftButtonCompletion))
        self.addAction(UIAlertAction(title: L10n.Buttons.ok, style: .default, handler: rightButtonCompletion))
    }
}

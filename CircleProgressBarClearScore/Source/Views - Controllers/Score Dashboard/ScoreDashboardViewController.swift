//
//  ScoreDashboardViewController.swift
//  CircleProgressBarClearScore
//
//  Created by Javier Servate on 11/04/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class ScoreDashboardViewController: UIViewController {

    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var yourScoreIsLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var outOfLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.title = L10n.Labels.NavigationBar.scoreDashboard
        
        setStyle()
        retrieveScore()
    }
    
    private func setStyle() {
        circularView.layer.cornerRadius = circularView.frame.height / 2
        circularView.layer.masksToBounds = true
        circularView.layer.borderColor = UIColor.black.cgColor
        circularView.layer.borderWidth = 2
        
        yourScoreIsLabel.isHidden = true
        scoreLabel.isHidden = true
        outOfLabel.isHidden = true
    }
    
    private func retrieveScore() {
        //Using [weak self] to avoid memory leaks
        APIClient.shared.performRequest(router: APIRouter.score) { [weak self] (account: Account?) in
            guard let account = account else {
                let alert = TwoButtonsAlertViewController(title: L10n.Alert.Titles.error, message: L10n.Alert.Description.requestError, preferredStyle: .alert)
                alert.setupAlert(leftButtonCompletion: { [weak self] (_ action) in
                    return
                }, rightButtonCompletion: { [weak self] (_ action) in
                    self?.retrieveScore()
                })
                self?.present(alert, animated: true, completion: nil)
                return
            }
            let score = account.creditReportInfo.score
            self?.showInfo(score: score)
        }
    }
    
    func showInfo(score: Int) {
        yourScoreIsLabel.isHidden = false
        scoreLabel.isHidden = false
        outOfLabel.isHidden = false
        
        scoreLabel.text = String(score)
        showCircularProgress(score: score)
    }
    
    private func showCircularProgress(score: Int) {
        
        let startPoint = CGFloat(DegreesConverter().formatToRadians(degress: 270))
        let radianFullCircle = CGFloat(DegreesConverter().formatToRadians(degress: 270))
        let scorePercentage = CGFloat(score) / 1000
        let finalPoint = radianFullCircle * scorePercentage
        
        let circlePath = UIBezierPath(arcCenter: self.circularView.center, radius: CGFloat((self.circularView.frame.height / 2) - 10), startAngle: startPoint, endAngle: finalPoint, clockwise: true)
        
        let innerCircle = CAShapeLayer()
        innerCircle.path = circlePath.cgPath
        innerCircle.lineWidth = 8
        innerCircle.fillColor = UIColor.clear.cgColor
        innerCircle.strokeColor = UIColor.blue.cgColor
        innerCircle.strokeEnd = 0.0
        
        let backgroundGradient = CAGradientLayer()
        backgroundGradient.colors = [#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor, #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1).cgColor, UIColor.black.cgColor]
        backgroundGradient.locations = [0.0, 0.5, 1.0]
        backgroundGradient.frame = view.bounds
        backgroundGradient.mask = innerCircle
        view.layer.addSublayer(backgroundGradient)
        
        animateInnerCircle(duration: 1, layer: innerCircle)
    }
    
    func animateInnerCircle(duration: TimeInterval, layer: CAShapeLayer) {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = duration
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        layer.strokeEnd = 1.0
        layer.add(animation, forKey: "animateCircle")
    }
}

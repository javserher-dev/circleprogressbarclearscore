//
//  CircleProgressBarClearScoreUITests.swift
//  CircleProgressBarClearScoreUITests
//
//  Created by Javier Servate on 12/04/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import XCTest

class CircleProgressBarClearScoreUITests: XCTestCase {
    
    var testApp = XCUIApplication()

    override func setUp() {
        testApp.launch()
    }

    override func tearDown() {
        testApp.terminate()
    }
    
    func isShowingDashboard() {
        let dashboardView = testApp.navigationBars.element(boundBy: 1).staticTexts["Dashboard"]
        XCTAssertTrue(dashboardView.waitForExistence(timeout: 1))
    }
}

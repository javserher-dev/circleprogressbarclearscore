//
//  CircleProgressBarClearScoreTests.swift
//  CircleProgressBarClearScoreTests
//
//  Created by Javier Servate on 12/04/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import XCTest
@testable import CircleProgressBarClearScore

class CircleProgressBarClearScoreTests: XCTestCase {
    
    var degreesConverter = DegreesConverter()
    var dashboard = ScoreDashboardViewController()

    override func setUp() {
        
    }
    
    func setsScoreValueInLabelTest() {
        dashboard.showInfo(score: 100)
        XCTAssertTrue(dashboard.scoreLabel.text == "100")
    }
    
    func convertsRadiansTest() {
        let radians = Double.pi * 2
        let degrees = degreesConverter.formatToDegrees(radians: radians)
        XCTAssertTrue(degrees == 360)
    }
}
